var toUpperText = document.querySelectorAll('.first-block .name-lection .font-style-jost');
toUpperText.forEach(function(elem) {
    elem.innerHTML = elem.innerHTML.toUpperCase();
});

var textLengthLimited = document.querySelectorAll('.first-block .name-lection>.description-name');

textLengthLimited.forEach(function(elem) {
    if (elem.innerHTML.length > 20) {
        var text = elem.innerHTML.slice(0,20);
        text += '...';

        elem.innerHTML = text;
    }
});