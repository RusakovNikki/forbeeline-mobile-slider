var dataSet = {}
var dataName = document.querySelectorAll('.item[data-group]');
dataName.forEach(function(elem) {
    var dataItem = {
        title: elem.querySelector('.font-style-jost').innerText,
        description: elem.querySelector('.description-name').innerText,
        date: elem.querySelector('.data-lection').innerText,
        image: elem.querySelector('.image-item img').getAttribute("src"),
        label: elem.querySelector('.whatTheVideo').innerText,
    };
    if (!dataSet[elem.getAttribute("data-group")]) {
        dataSet[elem.getAttribute("data-group")] = [dataItem];
    } else {
        dataSet[elem.getAttribute("data-group")].push(dataItem);
    }
    
})
    
console.log(JSON.stringify(dataSet, null, 2));