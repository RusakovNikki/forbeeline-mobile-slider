// $(function(){
  
//   $('li.dropdown > a').on('click',function(event){
    
//     event.preventDefault()
    
//     $(this).parent().find('ul').first().toggle(300);
    
//     $(this).parent().siblings().find('ul').hide(200);
    
//     //Hide menu when clicked outside
//     $(this).parent().find('ul').mouseleave(function(){  
//       var thisUI = $(this);
//       $('html').click(function(){
//         thisUI.hide();
//         $('html').unbind('click');
//       });
//     });
//   });
// });

var buttons = document.querySelectorAll('.onclick');
buttons.forEach(function(elem) {
  elem.onclick = function() {
    var ul = elem.nextElementSibling;
    if (!ul) return 0;
    var arrow = elem.querySelector('.checkmark');

    
    if (ul.classList.contains('is-active')) {
      ul.classList.remove('is-active');
      arrow.style.transition = '0.4s';
      arrow.style.transform = 'rotate(0deg)';
    }
    else {
      ul.classList.add('is-active');
      arrow.style.transition = '0.4s';
      arrow.style.transform = 'rotate(180deg)';
    }
  }
  
});

// $(document).ready(function(){

//   $('.onclick').click(function(){
//     $('.dropdown-menu').toggle('slow');
//   });
  
// });