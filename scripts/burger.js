var openMenu = document.querySelector('.open-menu-block');
var accPhoto = document.querySelector('.accaunt-photo');
var nameProfile = document.querySelector('.name-profile');
var online = document.querySelector('.online');
var proAccaunt = document.querySelector('.pro-accaunt');

openMenu.onclick = function() {
	if (openMenu.className === 'open-menu-block') {
		document.getElementsByTagName('aside')[0].style.display = 'block';
		document.querySelector('.logo-place').style.position = 'absolute';
		this.className = 'close';
		this.style.marginLeft = '180px';
		this.style.marginTop = '20px';
		accPhoto.style.display = 'none';
		nameProfile.style.display = 'none';
		online.style.display = 'none';
		proAccaunt.style.display = 'none';
		return 0;
	}
	if (openMenu.className === 'close') {
		document.getElementsByTagName('aside')[0].style.display = 'none';
		this.className = 'open-menu-block';
		this.style.marginLeft = '0px';
		this.style.marginTop = '0px';

		accPhoto.style.display = 'block';
		nameProfile.style.display = 'block';
		online.style.display = 'block';
		proAccaunt.style.display = 'block';
		return 0;
	}
}

