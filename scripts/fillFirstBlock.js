window.onload = function() {
    function countLection(elem) {
        document.querySelector('.first-block .viev-btn').innerHTML = elem.length + ' лекций';
    }
    function delItems() {
        block.querySelectorAll('.item').forEach(function(elem) {
            block.removeChild(elem);
        });
    }
    function showAllItems(items) {
        delItems();
        items.forEach(elem => {block.appendChild(elem.cloneNode(true));});
        sliderOne();
        countLection(elements);
    }
    function sortByTopic() {
        var textSort = this.classList.contains('viev-btn-html') ? 'html' : this.classList.contains('viev-btn-css')? 'css' : 'javascript';
        var itemSortByData = document.querySelectorAll(`.second-block .item[data-group="${textSort}"]`);
        delItems();
        showAllItems(itemSortByData);
        sliderOne();
    }

    var block = document.querySelector('.first-block .slider-track');
    var elements = document.querySelectorAll('.item');
    sliderOne(); // отвечает за слайдер
    showAllItems(elements);
    countLection(elements);

    document.querySelector('.viev-btn-html').onclick = sortByTopic;
    document.querySelector('.viev-btn-css').onclick = sortByTopic;
    document.querySelector('.viev-btn-javascript').onclick = sortByTopic;
    document.querySelector('.first-block .viev-btn').onclick = function() {showAllItems(elements)};
}
