function sliderOne() {
    var btnNext = document.querySelectorAll('.btn-next');
    var btnPrev = document.querySelectorAll('.btn-prev');
    function Slider(arg = "") {
        var index = 0, track, items;
        var itemWidth = document.querySelector('.second-block .item-conteiner').clientWidth / 4;
        
        function btnPrevElement() {
            track = this.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild;
            items = this.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild.children;
            let itemsLeft = Math.abs(index) / itemWidth;
            index += itemsLeft >= 1 ? itemWidth : itemsLeft * itemWidth;
            changeImage()
        }
        
        function btnNextElement() {
            track = this.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild;
            items = this.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild.children;
            let itemsLeft = items.length - (Math.abs(index) + 4 * itemWidth) / itemWidth;
            if (items.length <= 4) itemsLeft = 0;
            index -=itemsLeft >= 1 ? itemWidth : itemsLeft * itemWidth;
            changeImage()
        }
        
        function changeImage() {
            track.style.trasition = '0.2s';
            track.style.transform = `translateX(${index}px)`;
        }
    
        return {
            prev: btnPrevElement,
            next: btnNextElement,
        }
    }
    
    for (let i = 0; i < btnNext.length; i++) {
        var obj = {};
        obj[`slider${i}`] = Slider();
        obj.btnNext = btnNext[i];
        obj.btnPrev = btnPrev[i];
        obj.btnNext.onclick = obj[`slider${i}`].next;
        obj.btnPrev.onclick = obj[`slider${i}`].prev;
    }
}
sliderOne();